# Projectes 2017


## Escola del Treball de Barcelona


### ASIX Administració de sistemes informàtics en xarxa


#### Curs 2016-2017


#### M14 Projectes


1) Guillermo Ferrer:  **Journald TLS**

https://github.com/guibos/projecte 


2) Ivan Madero: **Systemd pot substituir cron?**


https://github.com/Ivan-Madero/proyecto-final 


3) Abdrà Soto: **Màquina Quiosc amb systemd**




4) Brian Mengibar: **Serveis informatius de systemd**


https://github.com/brianmengibar/projecte-final 


5) Bruno Mondelo: **Alternatives manuals al systemd**


https://github.com/mondelob/manual-systemd-unit-execution 


6) Omar Asensio: **Gestió de logs de l’escola**

https://github.com/soulbiz/logging-elk 


7) Andres Usma: **Gestió de logs de l’escola**

https://github.com/andresfelipeusma/projectekibana 


8) Pedro Romero: **LDAP / Docker  TLS**

Github        https://github.com/antagme/

Docker Hub    https://hub.docker.com/u/antagme/


9) Pablo Prieto: **LDAP / Docker  TLS**

Github: https://github.com/pdocker 


